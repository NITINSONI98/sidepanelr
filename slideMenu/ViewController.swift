//
//  ViewController.swift
//  slideMenu
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnLeftSlide: UIButton!
    @IBOutlet weak var btnRightSlide: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func btnRightSide(_ sender: UIButton) {
         sender.isEnabled = false
         Appearance.ShowPanel(obj: self,opr: .right,identifier: "ViewControllerRightSlide")
         sender.isEnabled = true
    }
    @IBAction func btnLeftSlide(_ sender: UIButton) {
         sender.isEnabled = false
         Appearance.ShowPanel(obj: self,opr: .left,identifier: "ViewControllerLelfSlide")
         sender.isEnabled = true
    }
}
